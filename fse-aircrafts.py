#! /usr/bin/python3

# © 2019 Yves-Alexis Perez <corsac@corsac.net>
# SPDX-License-Identifier: MIT
# Aircraft sales check script for FSEconomy (http://fseconomy.net)

# First download the relevant data source at
# http://server.fseconomy.net/data?format=csv&query=aircraft&search=forsale&userkey=<USERKEY>

import argparse, sys, pandas, geopy.distance
pandas.set_option('display.expand_frame_repr',False)

def filter_distance(row, dest, max_distance, icaodf):
    source = row['Location']
    if source == "In Flight": return True
    source_coord = tuple(icaodf.loc[source, 'lat':'lon'])
    dest_coord = tuple(icaodf.loc[dest, 'lat':'lon'])
    distance = geopy.distance.distance(source_coord, dest_coord).nm
    return distance <= max_distance

def main():
    parser = argparse.ArgumentParser(description="Find aircraft for sale")
    parser.add_argument('-a', '--aircraft', help="Aircraft model from FSE")
    parser.add_argument('-l', '--location', help="ICAO code for nearest airport")
    parser.add_argument('-d', '--distance', help="Distance from ICAO, default 100nm", default=100, type=int)
    parser.add_argument('-r', '--registration', help="Aircraft Registration ID")
    parser.add_argument('-s', '--serial', help="Aircraft serial number in FSE", type=int)
    parser.add_argument('-p', '--price', help="Max price", type=int)
    args = parser.parse_args()

    result = pandas.read_csv("fse.csv", index_col=0)

    if args.aircraft:
        result = result[result['MakeModel'] == args.aircraft]

    if args.location:
        icaodf = pandas.read_csv("icaodata.csv", index_col=0)
        result = result[result.apply(filter_distance, args=(args.location, args.distance, icaodf), axis=1)]

    if args.registration:
        result = result[result['Registration'] == args.registration]

    if args.price:
        result = result[result['SalePrice'] <= args.price]

    if args.serial:
        result = result.loc[[args.serial]]

    if result.empty: return

    result = result.sort_values(by=['SalePrice'])
    if args.aircraft and args.distance and args.location:
        print(args.aircraft + " for sale less than " + str(args.distance) + "nm from " + args.location)
    print(result[["Registration",
                  "Owner",
                  "Location",
                  "LocationName",
                  "SalePrice",
                  "Equipment",
                  "PctFuel",
                  "AirframeTime",
                  "EngineTime",
                  "TimeLast100hr"
                  ]])

if __name__ == "__main__":
    main()
