#! /usr/bin/python3

# © 2019 Yves-Alexis Perez <corsac@corsac.net>
# SPDX-License-Identifier: MIT
# Aircraft sales check script for FSEconomy (http://fseconomy.net)

import sys, pandas, slugify, matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

columns=[ 'date', 'min', 'max', 'mean', 'median' ]

fsedf = pandas.read_csv("fse.csv", usecols=['MakeModel', 'SalePrice'])
for makemodel in fsedf.MakeModel.unique():
    makemodeldf = fsedf[fsedf['MakeModel'] == makemodel]

    csv = "history/" + slugify.slugify(makemodel) + ".csv"
    png = "images/" + slugify.slugify(makemodel) + ".png"

    # load history or create a new file
    try:
            df = pandas.read_csv(csv)

    except FileNotFoundError:
            df = pandas.DataFrame(columns=columns)
            df.to_csv(csv, index=False)


    # Append new data
    min    = makemodeldf['SalePrice'].min()
    max    = makemodeldf['SalePrice'].max()
    mean   = makemodeldf['SalePrice'].mean()
    median = makemodeldf['SalePrice'].median()
    new = pandas.DataFrame([[pandas.to_datetime('now'), min, max, mean, median]], columns=columns)
    df = df.append(new, ignore_index=True)

    # Save to history
    df.to_csv(csv, index=False)

    # Set date column as index
    df['date'] = pandas.to_datetime(df['date'])
    df.index = df['date']
    del df['date']

    # Generate the image
    ax = df.plot()
    ax.set_xticklabels([pandas_datetime.strftime("%d/%m/%y") for pandas_datetime in df.index])
    plt.suptitle(makemodel)
    plt.savefig(png)
    plt.close()
